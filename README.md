# Store

Store is a simple abstraction for key-value stores. It allows easier use of these stores, and makes it easier to switch to other stores using the same interface.