package store

import "github.com/vmihailenco/msgpack/v5"

// Store represents a key value store
type Store interface {
	// Set sets a key to a value
	Set(key string, val interface{}) error
	// Get places a value at a key into val
	Get(key string, val interface{}) error
	// GetItem gets an item from the store
	GetItem(string) (*Item, error)
	// Delete deletes a value from the store
	Delete(string) error
	// Modify allows editing of a value in the store
	Modify(string, ModifyFunc) error
	// Bucket returns a bucket with the specified name
	Bucket(name string) Bucket
	// Iter returns an iterator
	Iter() Iter
}

// Bucket represents a bucket within a store
type Bucket interface {
	// Name returns the bucket name
	Name() string
	// Set sets a key in the bucket to a value
	Set(string, interface{}) error
	// Get places a value at a key in the bucket into val
	Get(string, interface{}) error
	// GetItem gets an item from the store
	GetItem(string) (*Item, error)
	// Delete deletes a value from the bucket
	Delete(key string) error
	// Modify allows editing of a value in the bucket
	Modify(string, ModifyFunc) error
	// Iter returns an iterator for the bucket
	Iter() Iter
}

type ModifyFunc func(*Item) (interface{}, error)

// Item represents an iterator item
type Item struct {
	Key  string
	Data []byte
}

// Value sets out to the value of the item data
func (it *Item) Value(out interface{}) error {
	return msgpack.Unmarshal(it.Data, out)
}

// Iter represents an iterator channel
type Iter <-chan *Item
